from random import randint

print('-=' * 10)
print('JOGO DO PAR OU ÍMPAR')
v = 0
while True:
    print('-=' * 10)
    print('')
    a = int(input('Digite um número: '))
    pc = randint(0,11)
    total = a + pc
    pimpa = ' '
    while pimpa not in 'PI':
        pimpa = str(input('Você Quer Par ou Ímpar? [P/I]: ')) . strip().upper()[0]
    print('')
    print(f'Você jogou {a} e o computador jogou {pc}')
    print(f'O total foi {total}')
    print('')
    if pimpa == 'P':
        if total % 2 == 0:
            print('\033[1;32mVocê VENCEU!\033[m')
            v += 1
        else:
            print('\033[1;31mVocê PERDEU!\033[m')
            break
    elif pimpa == 'I':
        if total % 2 == 1:
            print('\033[1;32mVocê VENCEU!\033[m')
            v += 1
        else:
            print('\033[1;31mVocê PERDEU!\033[m')
            break
    print('Vamos jogar novamente...')
    print('')
print(f'GAME OVER! Você venceu {v} vezes.')